# Coding Problem - Onboarding Hospitals by Customer Success team #

OnwardHealth is a fast-growing startup company in the healthcare space leveraging technology. As much as the startup is growing, it has some unique challenges in Hyderabad. Well, you know the rapid rate at which infrastructure development is happening and it comes with a temporary hardships of narrowed road leading to increased traffic. And we can't stop until just about everything settles down around us. We've got a lot of Hospitals in and around Hyderabad as our clients. They need help on-boarding their patients data into our system. For this our customer-success team members visits various hospitals everyday. In this endeavour, he/she manually figures out an optimal route to be taken, to travel to various Hospitals to make the best use of operational time.

You as a good friend of this team, want to put your technical prowess to solve this problem. For the purpose of this problem statement, you assume that the route map in Hyderabad is one-way only. Now that means a direct route from Gachobowli to Madhapur does not imply the existence of a direct route from Madhapur to Gachibowli. If both these route exists, then they are distinct and not necessarily of same distance.

The buddies at customer success team reach out to you providing information about possible routes between various points/hospitals.
You as geek got to solve this problem by calculating:

* the distance along certain routes
* the number of different routes between two points/hospitals
* the shortest route between two hospitals/points

The input to the program consists of touples, each in the format like AB5 where

A is the starting point

B is the end point

5 is the distance unit between start and end points

It is important that a given route should appear not more than once. And for a given route, the start and end points will not be the same hospital. For the test input the location points or hospitals are named A to E. A route from A to B with a distance of 5 units is represented as AB5. For questions, where no route exists, output "NO SUCH ROUTE".

Your program should be able to answer the following questions:

1. The distance of the route A-B-C
2. The distance of the route A-E-B-C-D
3. The distance of the route A-E-D.
4. The number of trips starting at C and ending at C with a maximum of 3 stops. In the sample data below, there are two such trips: C-D-C (2 stops) and C-E-B-C (3 stops).
5. The number of trips starting at A and ending at C with exactly 4 stops. In the sample data below, there are three such trips: A to C (via B, C, D); A to C (via D, C, D); and A to C (via D, E, B).
6. The length of the shortest route (in terms of distance to travel) from A to C.
7. The length of the shortest route (in terms of distance to travel) from B to B.
8. The number of different routes from C to C with a distance of less than 30. In the sample data, the trips are: CDC, CEBC, CEBCDC, CDCEBC, CDEBC, CEBCEBC, CEBCEBCEBC.


### Test Input ###

AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7

### Expected Test Output ###

1. 9
2. 22
3. NO SUCH ROUTE4. 2
5. 3
6. 9
7. 9
8. 7


### Requirements ###

1. OnwardHealth really values polyglot developers. But for the purpose of this assignment, you're expected to use Java (preferably 8, but that is optional) for your solution.
2. Do not use any 3rd-party libraries to solve the problem. You may however use external libraries or tools for building or testing purposes, such as JUnit, Mockito, etc.
3. There must be a way to supply the application with input data.
4. The code must compile. And the application must run.
5. Your solution must work correctly against the supplied test data.
6. Write production quality code. It does matter for us.
7. Publish your code in Bitbucket, and share us the link of your project.
8. At the top level of your project, create a short ReadMe.md file. Ideally it should contain information about your code, design, any assumptions you made and also instructions on how to run your application.
9. We allow 3 days from the date that you receive these instructions to submit your code, but you may request more time if needed.
10. Have a problem in submitting a solution? Do reach out to us.